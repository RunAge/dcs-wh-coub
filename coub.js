const fetch = require('node-fetch');

class Coub {
  constructor(id) {
    return (async () => {
      this.id = id;
      this.JSON = await this.getJSON();
      this.tags = this.JSON.tags;
      this.permalink = this.JSON.permalink;
      this.title = this.JSON.title;
      this.author = this.JSON.channel;
      this.mediaBlocks = this.JSON.media_blocks;
      return this;
    })();
  }

  async getJSON() {
    const BASE_URI = 'https://coub.com/api/v2/coubs/';
    const coub = await fetch(BASE_URI + this.id);
    return coub.json();
  }
}

module.exports = Coub;
