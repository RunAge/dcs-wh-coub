const parser = require('js-video-url-parser');
const Discord = require('discord.js');
const debug = require('debug')('UtilsBot')
const Coub = require('./coub');

const logBot = debug.extend('Discord');
const logWH = debug.extend('Webhook');
const WebhookClient = new Discord.WebhookClient(process.env.DSCWH_ID, process.env.DSCWH_TOKEN)
const Client = new Discord.Client({
  messageCacheMaxSize: 0,
  messageCacheLifetime: 1,
  messageSweepInterval: 0,
  fetchAllMembers: false,
  presence: {
    status: 'invisible',
  },
  disabledEvents: [
    // 'READY',
    // 'RESUMED',
    // 'GUILD_CREATE',
    // 'GUILD_DELETE',
    // 'GUILD_UPDATE',
    'GUILD_MEMBER_ADD',
    'GUILD_MEMBER_REMOVE',
    'GUILD_MEMBER_UPDATE',
    'GUILD_MEMBERS_CHUNK',
    // 'GUILD_INTEGRATIONS_UPDATE',
    'GUILD_ROLE_CREATE',
    'GUILD_ROLE_DELETE',
    'GUILD_ROLE_UPDATE',
    'GUILD_BAN_ADD',
    'GUILD_BAN_REMOVE',
    'GUILD_EMOJIS_UPDATE',
    'CHANNEL_CREATE',
    'CHANNEL_DELETE',
    'CHANNEL_UPDATE',
    'CHANNEL_PINS_UPDATE',
    // 'MESSAGE_CREATE',
    'MESSAGE_DELETE',
    'MESSAGE_UPDATE',
    'MESSAGE_DELETE_BULK',
    // 'MESSAGE_REACTION_ADD',
    'MESSAGE_REACTION_REMOVE',
    'MESSAGE_REACTION_REMOVE_ALL',
    'USER_UPDATE',
    'USER_SETTINGS_UPDATE',
    'PRESENCE_UPDATE',
    'TYPING_START',
    'VOICE_STATE_UPDATE',
    'VOICE_SERVER_UPDATE',
    // 'WEBHOOKS_UPDATE',
  ],
});

Client.on('ready', () => {
  logBot('Ready!')
})

Client.on('message', async (message)=> {
  const parsed = parser.parse(message.content);
  if(message.channel.id !== process.env.DCS_CHANNEL) return;
  if(!parsed || parsed.provider !== 'coub') return;
  const coub = await new Coub(parsed.id);
  const coubTags = [];
  coub.tags.forEach(tag => {
    coubTags.push(tag.title);
  })
  const embed = {
    title: coub.title,
    description: 'Tags: '+coubTags.join(', '),
    url: `https://coub.com/view/${coub.permalink}`,
    footer: {
      text: `Sended by ${message.author.username}${message.author.discriminator}`,
    },
    author: {
      name: coub.author.title,
      url: `https://coub.com/${coub.author.permalink}`,
      icon_url: coub.author.avatar_versions.template.split('%{version}').join('profile_pic_new'),
    }
  }
  try {
    const whMsg = await WebhookClient.send('', {embeds: [embed]});
    logWH(whMsg);
  } catch(err) {
    logWH(err);
  }

})

Client.login(process.env.DSC_TOKEN)
